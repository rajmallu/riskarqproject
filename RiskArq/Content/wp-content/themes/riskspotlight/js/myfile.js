$(document).ready(function(){
	$('#calltoaction').hide();

	$(window).scroll(function () {
	   var down = $(this).scrollTop() > 200;
	   var up =  $(this).scrollTop() < 1500;
	   
	   if ( down && up ) {
	       $('#calltoaction').slideDown();
	   } else {
	       $('#calltoaction').slideUp();
	   }
	   
	});

	$('.grid').masonry({
	  itemSelector: '.grid-item'
	});
});

